#! /usr/bin/ruby
require "rubygems"
require "xmpp4r"
require "xmpp4r/pubsub"
require "xmpp4r/pubsub/helper/servicehelper.rb"
require "xmpp4r/pubsub/helper/nodebrowser.rb"
require "xmpp4r/pubsub/helper/nodehelper.rb"
require 'xmpp4r/roster'
include Jabber
Jabber::debug = false

#A simple publisher, that publishes every message he receives to his updates pubsub node
class Publisher
  def initialize  username, password, host, port
    @log = Logger.new(STDOUT)
    @log.level = Logger::DEBUG
    @username = username
    @password = password
    @host = host
    @port = port
    @jid = "#{@username}@#{@host}/SLAC Information Publisher"
    @pubsubservice = "pubsub.#{@host}"
    @node = "home/#{@host}/#{@username}/updates"
    @client = Client.new(JID.new(@jid))
    @time = Time.now
  end

  #connect to server is always the first step to be awesome
  def connect
    @client.connect(@host, @port)
    begin #in case this doesn't work register a new user
      @client.auth(@password)
    rescue
      @client.close
      @client.connect(@host, @port)
      @client.register(@password, {'nick'=>@username,'name'=>@username})
    end
    @presence = Jabber::Presence.new
    @presence.show = :chat
    @presence.status = "Up and running!"
    @client.send(@presence)
    sleep(1)
    #get a roster helper
    @roster = Roster::Helper.new(@client)
    #spend pubsub a margaritha
    @pubsub = PubSub::ServiceHelper.new(@client, @pubsubservice)
    createnode @node
    addmessagecallback
    @log.info "#{@username} - Connected and ready to go!"
  end

  #publishes an update on the updates node
  def publishupdate text
    pubsub = PubSub::ServiceHelper.new(@client, @service)
    item = Jabber::PubSub::Item.new
    xml = REXML::Element.new("update")
    xml.text = text
    item.add(xml)
    pubsub.publish_item_to(@node, item)
    @log.info "#{@username} - Published to updates: #{text}"
  end

  #adds a callback for incoming messages
  def addmessagecallback
    @client.add_message_callback do |m|
      @log.info "#{@username} - Incoming message from: #{m.from}"
      @log.info "#{@username} - Message text: #{m.body}"
      #publish incoming message to updates node
      publishupdate m.body
    end
    @log.debug "#{@username} - Added message callback."
  end

  #create our pubsub node
  def createnode node
    begin
      @pubsub.create_node("home/#{@host}/#{@username}/")
    rescue
    end #if this fails, that normaly means the node has already been created
    begin
      @log.info "#{@username} - Feeding node: #{node}"
      @pubsub.create_node(node)
    rescue #if this fails, that normaly means the node has already been created
    end
  end
end


