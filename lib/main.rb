require 'Qt4'
require 'slacclient'
require 'gtk2'

#setup stream sources
Icestreamer.new '../test/Pflaumenbaum.mp3', 'ogg', 's1', 'krusty.techfak.uni-bielefeld.de', 9070
Icestreamer.new '../test/electro.mp3', 'ogg', 's2', 'krusty.techfak.uni-bielefeld.de', 9070
Icestreamer.new '../test/wipeout.mp3', 'ogg', 's3', 'krusty.techfak.uni-bielefeld.de', 9070

class Mixer
  attr_accessor :sounds

  def initialize
    puts "Now we connect to an icecast server with a SLAC 1"
    @slacclient = SLACclient.new 'http://krusty.techfak.uni-bielefeld.de:9070'
    @sounds = {}
  end

  def add_sound_source id, sound_source
    @sounds[id] = sound_source
    @slacclient.add_stream(sound_source.stream_id)
    @slacclient.stream_volume(sound_source.stream_id, 0)
  end

  def set_position x,y
    @sounds.each_value do |ss|
      @slacclient.stream_volume(ss.stream_id, ss.get_rel_vol(x,y))
      @slacclient.stream_panorama(ss.stream_id, ss.get_rel_panner(x,y))
    end
  end
end

class SoundSource
  @@rvol2 = 100 # distance at which volume is perceived as only half
  attr_accessor :x, :y, :vol, :stream_id, :rel_vol, :rel_angle, :rel_panner

  def initialize stream_id, vol=1, x=0, y=0
    @stream_id = stream_id
    @vol, @x, @y= vol, x, y
  end

  #get the relative volume
  def get_rel_vol x,y
    dist = Math.sqrt((x-@x)**2 + (y-@y)**2)
    @rel_vol = @@rvol2**2.5 / (dist**2.5 + @@rvol2**2.5)
    return @rel_vol
  end

  #get the relative angle in [-1..1] (for stereo panner)
  def get_rel_panner x,y
    #angle in radian (up -0.5, left -1/1, down 0.5, right 0)
    @rel_panner = Math.atan2(@y-y,@x-x)/Math::PI
    #we want a range from -1 to +1
    @rel_panner = -2*@rel_panner.abs + 1
    #puts "#{@stream_id} relative angle: #{@rel_angle}"
    return @rel_panner
  end

  #get the relative angle
  def get_rel_angle x,y
    #angle in radian (up -0.5, left -1/1, down 0.5, right 0)
    @rel_angle = Math.atan2(@y-y,@x-x)/Math::PI
    #we want a range from -1 to +1
    @rel_angle = @rel_angle * 180
    return @rel_angle
  end
end

class DragInfo
  attr_accessor :x, :y, :object

  def initialize startx, starty, object=nil
    @x, @y, @object = startx, starty, object
  end
end

class Gui < Qt::Widget
  @@r = 10
  def initialize(mixer, parent = nil)
    super()
    @mixer = mixer
    setPalette(Qt::Palette.new(Qt::Color.new(250, 250, 200)))
    setAutoFillBackground(true)
    @x,@y=0,0
    @cur1 = self.cursor
    @cur2 = Qt::Cursor.new(Qt::PointingHandCursor)
    self.mouseTracking = true
  end

  def mousePressEvent event
    @draginfo = nil
    if event.buttons == Qt::RightButton
      update_listener event.x, event.y
    elsif event.buttons == Qt::LeftButton
      ss = object_at(event.x, event.y)
      @draginfo = DragInfo.new(event.x, event.y, ss) unless ss.nil?
    end
  end

  def object_at x,y
    @mixer.sounds.each_value do |ss|
      return ss if (ss.x-x)**2 + (ss.y-y)**2 <= @@r**2
    end
    return nil
  end

  def mouseReleaseEvent event
    update_drag_object event.x, event.y unless @draginfo.nil?
  end

  def update_drag_object x,y
    @draginfo.object.x = x
    @draginfo.object.y = y
    @mixer.set_position(@x, @y)
    update
  end

  def update_listener x,y
    @x = x
    @y = y
    @mixer.set_position(@x, @y)
    update
  end

  def mouseMoveEvent event
    self.cursor = object_at(event.x, event.y).nil? ? @cur1 : @cur2
    if event.buttons == Qt::LeftButton and not @draginfo.nil?
      update_drag_object event.x, event.y unless @draginfo.nil?
    elsif event.buttons == Qt::RightButton
      update_listener event.x, event.y
    end
  end

  def paintEvent(event)
    painter = Qt::Painter.new(self)
    painter.setRenderHint(Qt::Painter::Antialiasing)
    painter.setBrush(Qt::Brush.new(Qt::Color.new(0,0,255,150)))
    @mixer.sounds.each do |id,ss|
      painter.resetMatrix
      painter.translate(ss.x,ss.y)
      painter.setPen(Qt::NoPen)
      painter.drawPie(Qt::Rect.new(-@@r,-@@r, 2*@@r, 2*@@r), 0, 360 * 16)
      painter.setPen(Qt::Pen.new(Qt::blue))
      painter.drawText(@@r+2,@@r+2,"id: #{id} vol: #{ss.rel_vol.to_s[0..3]} pos: #{ss.rel_panner.to_s[0..3]}")
    end
    painter.resetMatrix
    painter.translate(@x,@y)
    painter.setPen(Qt::NoPen)
    painter.setBrush(Qt::Brush.new(Qt::Color.new(255,0,0,150)))
    painter.drawPie(Qt::Rect.new(-@@r,-@@r, 2*@@r, 2*@@r), 0, 360 * 16)
    painter.end()
  end
end

mix = Mixer.new
mix.add_sound_source('1', SoundSource.new('s1',1,10,10))
mix.add_sound_source('2', SoundSource.new('s2',1,400,10))
mix.add_sound_source('3', SoundSource.new('s3',1,400,400))

app = Qt::Application.new(ARGV)
gui = Gui.new mix
gui.resize(600, 600)
gui.show()
app.exec()