require 'rubygems'
require 'gst'
require 'gnome2'


#this class streams an audiostream to our Icecast server
class Icestreamer

  attr_accessor :format
  attr_accessor :pipeline, :thread
  attr_accessor :clientname, :iceserver, :password, :mount

  def initialize filename, format, clientname, ip, port
    unless File.exist? filename
      puts "File #{filename} not found, sorry."
    end
    unless format == 'ogg' || format == 'mp3'
      puts "Format #{@format} not recoginzed, sorry."
    end
    @format = format
    #The streamurl will end with clientname + format
    @clientname = clientname
    Gst.init
    #stuff we need to set manually until now
    @ip = ip
    @mount = "#{@clientname}.#{@format}"
    @password = "source!secret"
    # create a new pipeline to hold the elements
    @pipeline = Gst::Pipeline.new
    # create a disk reader (we could also get microphone input here)
    @reader = Gst::ElementFactory.make("filesrc", "reader")
    @reader.location = filename
    # now it's time to get the decoder (decodebin is auto-magical)
    @decoder = Gst::ElementFactory.make("decodebin")
    #and an audio convertor
    @audioconverter = Gst::ElementFactory.make("audioconvert")
    # we pack everything in a nice little ogg or mp3
    @audioencoder = Gst::ElementFactory.make("lame","audioencoder") if format == 'mp3'
    @muxer = Gst::ElementFactory.make("mp3parse","muxer") if format == 'mp3'
    @audioencoder = Gst::ElementFactory.make("vorbisenc","audioencoder") if format == 'ogg'
    @muxer = Gst::ElementFactory.make("oggmux","muxer") if format == 'ogg'
    # and a sink, in this case our icecast server
    @s2s = Gst::ElementFactory.make("shout2send","shout2send")
    @s2s.port = port
    @s2s.ip, @s2s.mount, @s2s.password = @ip, @mount, @password
    # add objects to the main pipeline
    @pipeline.add(@reader, @decoder,@audioconverter,@audioencoder,@muxer,@s2s)
    # link elements
    @reader >> @decoder
    @audioconverter >> @audioencoder >> @muxer >> @s2s
    #now we need to connect the decoder and converter
    @decoder.signal_connect("new-decoded-pad") do |element, pad|
      sink_pad = @audioconverter.get_pad("sink")
      pad.link(sink_pad)
    end
    #for debug use
    #@audiosink = Gst::ElementFactory.make("pulsesink", "audiosink")
    #@pipeline.add(@reader, @decoder, @audiosink)
    #@reader >> @decoder >> audiosink
    @pipeline.play
  end

  #basic player methods
  def play
    @thread = Thread.new do
      @pipeline.play
    end
    @thread.run
    puts "Now streaming: #{@reader.location} to mount #{@mount} on icecast server #{@s2s.ip} at port #{@s2s.port}"
  end
  def pause
    @pipeline.pause
  end
  def stop
    @pipeline.stop
  end
end

#SLAC - Spatial Linked Audiostream Composition
#this is the first version of the actual SLAC client
class SLACclient

  attr_accessor :slacserver
  attr_accessor :streamsources

  def initialize slacserver
    @slacserver = slacserver
    @streamsources = Hash.new("no stream here!")
    Gst.init
  end

  # adds a new stream source to the pipeline
  def add_stream streamid
    puts "Adding new stream: #{@slacserver}/#{streamid}.ogg"
    streamhash = Hash.new("I'm empty.")
    @streamsources[streamid] = streamhash
    # create a new pipeline to hold the elements
    pipeline = Gst::Pipeline.new
    audiosink = Gst::ElementFactory.make("pulsesink","audiosink")
    # add objects to the main pipeline, all other objects will come later through add_stream
    pipeline.add(audiosink)
    pipeline.pause
    streamhash["pipeline"] = pipeline
    #we will put all our stuff into the bin
    streambin = Gst::ElementFactory.make("bin", streamid)
    streamhash["bin"] = streambin

    # the (icecast) audio source
    audiosrc = Gst::ElementFactory.make("neonhttpsrc","audiosrc")
    audiosrc.location = "#{@slacserver}/#{streamid}.ogg"
    # now it's time to get the decoder (decodebin is auto-magical)
    decoder = Gst::ElementFactory.make("decodebin","decoder")
    #and an audio convertor
    audioconverter = Gst::ElementFactory.make("audioconvert","audioconverter")
    #our volume control
    volume = Gst::ElementFactory.make("volume","volume")
    #initialize muted?
    volume.volume = 1.0
    #the level
    level = Gst::ElementFactory.make("level","level")
    #finally, the stereo panorama effect
    panorama = Gst::ElementFactory.make("audiopanorama","audiopanorama")
    #add the elements to the streambin
    streambin.add(audiosrc, decoder, audioconverter, panorama, volume, level )

    #link what can already be linked
    audiosrc >> decoder
    audioconverter >> panorama >> volume >> level
    puts "Initial linking done."

    #now we need to prepare the connection of decoder and converter (like decoder >> audioconverter)
    decoder.signal_connect("new-decoded-pad") do |element, pad|
      pipeline.pause
      sink_pad = audioconverter.get_pad("sink")
      pad.link(sink_pad)
      puts "Decoder and audioconverter linked."
      pipeline.play
      #streambin.sync_state_with_parent
    end

    puts "Now adding ghost pads."
    #inherit the level src pad for our bin
    level_src = level.get_pad("src")
    ghost_src = Gst::GhostPad.new("src", level_src)
    streambin.add_pad(ghost_src)
    puts "Ghost pads added."

    #pause while we do the last linking an adding
    pipeline.pause
    streambin.sync_state_with_parent
    pipeline.add(streambin)
    streambin >> audiosink

    #and finally play the stream again, if not already playing
    puts "Added new stream: #{@slacserver}/#{streamid}.ogg"
    pipeline.play
    streambin.sync_state_with_parent
    puts "Now playing SLAC stream with #{@streamsources.length} stream sources."
  end

  #adjust the volume of a specific stream
  def stream_volume streamid, volume
    unless @streamsources[streamid].nil?
      volume_element = @streamsources[streamid]["bin"].get_child("volume")
      volume_element.volume = volume
    end
  end

  #adjust the stereo position of a specific stream position (-1.0 left, 1.0 right)
  def stream_panorama streamid, panner
    return unless (panner >= -1.0 && panner <= 1.0)
    unless @streamsources[streamid].nil?
      panorama_element = @streamsources[streamid]["bin"].get_child("audiopanorama")
      panorama_element.panorama = panner
    end
  end

  #adjust the stereo rotator angle of a specific stream (rotation angle [-180 .. +180], positive is to the right)
  def stream_angle streamid, angle
    return unless (angle >= -180 && angle <= 180)
    unless @streamsources[streamid].nil?
      rotator_element = @streamsources[streamid]["bin"].get_child("stereo")
      #rotator_element.angle = angle
    end
  end
end