#! /usr/bin/ruby
require "rubygems"
require "xmpp4r"
require "xmpp4r/pubsub"
require "xmpp4r/pubsub/helper/servicehelper.rb"
require "xmpp4r/pubsub/helper/nodebrowser.rb"
require "xmpp4r/pubsub/helper/nodehelper.rb"
include Jabber

#A simple subscriber, that subscribes to a pubsub node
class Subscriber
  def initialize username, password, host, port,node
    @log = Logger.new(STDOUT)
    @log.level = Logger::DEBUG
    @username = username
    @password = password
    @host = host
    @port = port
    @jid = "#{@username}@#{@host}/SLAC Information Subscriber"
    @pubsubservice = "pubsub.#{@host}"
    @node = node
    @client = Client.new(JID.new(@jid))
    @time = Time.now
  end

  #connect to server is always the first step to be awesome
  def connect
    @client.connect(@host, @port)
    begin #in case this doesn't work register a new user
      @client.auth(@password)
    rescue
      @client.close
      @client.connect(@host, @port)
      @client.register(@password, {'nick'=>@username,'name'=>@username})
    end
    @presence = Jabber::Presence.new
    @presence.show = :chat
    @presence.status = "Up and running!"
    @client.send(@presence)
    sleep(1)
    #get a roster helper
    @roster = Roster::Helper.new(@client)
    @pubsub = PubSub::ServiceHelper.new(@client, @pubsubservice)
    sleep 2
    subscribe2node @node
    @log.info "#{@username} - Connected and ready to go!"
  end

  #subscribe to a pubsub node
  def subscribe2node node
    # subscribe to the node
    @pubsub = PubSub::ServiceHelper.new(@client, @service)
    @log.info "#{@username} - Subscribing to node: #{node}"
    @pubsub.subscribe_to(node)
    subscriptions = @pubsub.get_subscriptions_from_all_nodes()
    @log.info "#{@username} - subscriptions: #{subscriptions}\n\n"
    @log.info "#{@username} - events:\n"
    # set callback for new events
    @pubsub.add_event_callback {|event| nodeevent event}
  end

  #gets called when node received an event
  def nodeevent event
    begin
      event.payload.each do |e|
        @log.info e#,"----\n"
      end
    rescue
      #puts "Error : #{$!} \n #{event}"
    end
  end
end
