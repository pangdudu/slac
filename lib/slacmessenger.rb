#! /usr/bin/ruby
require "rubygems"
require "xmpp4r"
require "xmpp4r/pubsub"
require "xmpp4r/pubsub/helper/servicehelper.rb"
require "xmpp4r/pubsub/helper/nodebrowser.rb"
require "xmpp4r/pubsub/helper/nodehelper.rb"
require 'xmpp4r/roster'
include Jabber

class Pubsub

  def initialize username, password, host, port
    @log = Logger.new(STDOUT)
    @log.level = Logger::DEBUG
    #if we are the controller, this boolean but be true
    @controller = true if username.eql?("controller")
    @username = username
    @host = host
    @port = port
    @jid = "#{@username}@#{@host}/SLAC Information Client"
    @jid = "#{@username}@#{@host}/SLAC Information Controller" if @controller
    @password = password
    @pubsubservice = "pubsub.#{@host}"
    @updatesnode = "home/#{@host}/#{@username}/updates"
    @confignode = "home/#{@host}/#{@username}/config"
    @swarmnode = "home/#{@host}/#{@username}/swarm"
    # connect XMPP client
    @client = Client.new(JID.new(@jid))
    #if we want to generalwant to accept subscriptions
    @accept_subscription_requests = true
    #our fellow SWARM members
    @swarm = Hash.new("swarm is empty")
  end

  #connect to server is always the first step to be awesome
  def connect
    @client.connect(@host, @port)
    begin #in case this doesn't work register a new user
      @client.auth(@password)
    rescue
      @client.close
      register
    end
    @presence = Jabber::Presence.new
    @presence.show = :chat
    @presence.status = "Up and running!"
    @client.send(@presence)
    sleep(1)
    #get a roster helper
    @roster = Roster::Helper.new(@client)
    #spend pubsub a margaritha
    @pubsub = PubSub::ServiceHelper.new(@client, @pubsubservice)
    addcallbacks
    subscribe2jid "publisher@#{@host}" unless @controller
    getsubscriptions
    @log.info "#{@username} - Connected, subscribed and ready to go!"
  end

  #Now following are the callbacks, some may not be needed for what we do.
  #But isn't it allways to suffer some overhead to be informed. :)

  #will add all callbacks implemented in this class
  def addcallbacks
    addsubscriptioncallback
    addpresencecallback
    addpresencechangecallback
    addmessagecallback
    @log.info "#{@username} - All callbacks added. We're good to go!"
  end

  #adds callback for accepting incoming subscriptions, obvious isn't it. :)
  def addsubscriptioncallback
    @roster.add_subscription_request_callback do |item,presence|
      @log.debug "#{@username} - Incoming subscription request item: #{item.inspect} presence: #{presence.inspect}"
      if @accept_subscription_requests
        @roster.accept_subscription(presence.from)
        @log.debug "#{@username} - Accepted subscription request from #{presence.from}"
      else
        @roster.decline_subscription(presence.from)
        @log.debug "#{@username} - Declined subscription request from #{presence.from}"
      end
    end
    @log.debug "#{@username} - Added subscription callback."
  end

  #adds a callback for presence changes
  def addpresencecallback
    @roster.add_update_callback do |oldrosteritem,newrosteritem|
      @log.debug "#{@username} - Incoming roster update old item: #{oldrosteritem.inspect} new item: #{newrosteritem.inspect}"
      #@roster.add(newrosteritem) unless newrosteritem.nil?
      @log.debug "#{@username} - Trying to find #{newrosteritem.jid} returns #{@roster.find(newrosteritem.jid).inspect}"
      unless @roster.find(newrosteritem.jid).nil?
        @roster.find(newrosteritem.jid).each do |jid,rosteritem|
          @log.debug "#{@username} - JID #{jid} rosteritem #{rosteritem.inspect}"
          rosteritem.ask
          rosteritem.subscription = :both
        end
      end
    end
    @log.debug "#{@username} - Added presence callback."
  end

  #adds a callback for presence changes
  def addpresencechangecallback
    @roster.add_presence_callback do |rosteritem, old_presence, new_presence|
      @log.debug "#{@username} - Presence change of item: #{rosteritem.inspect} old presence: #{old_presence.inspect} new presence: #{new_presence.inspect}."
      #updateswarmerattribute new_presence.jid,"status","unknown"
    end
    @log.debug "#{@username} - Added presence change callback."
  end

  #adds a callback for incoming messages
  def addmessagecallback
    @client.add_message_callback do |m|
      #@log.debug "#{@username} - Pre - Incoming message: #{m.body} from: #{m.from}"
      begin
        #we filter out all pubsub messages, they are handled in the nodeevent method
        unless m.from.eql? @pubsubservice
          begin
            @log.debug "#{@username} - Incoming message: #{m.body} from: #{m.from}"
            #okay, the whole SWARM namespace stuff is kind of provisional
            swarmmessagehandler m if m.subject.include? "SWARM"
          rescue
            #@log.warn "#{@username} - Something went wrong while parsing a message."
            @log.warn "#{@username} - Didn't parse message body: #{m.body} from: #{m.from}"
          end
        end
      rescue
        @log.warn "#{@username} - Something went wrong while filtering out pubsub messages."
      end
    end
    @log.debug "#{@username} - Added message callback."
  end

  #handles the SWARM namespace messages (i.e. messages containing SWARM in their subject)
  def swarmmessagehandler message
    swarmconnecthandler message if message.subject.include? "CONNECT"
  end

  #handles the SWARM CONNECT messages, you kinda see where this is going?
  #this method will publish the updated list of swarm members
  def swarmconnecthandler message
    @log.debug "#{@username} - Incoming swarm connect message from #{message.from}!"
    addtoroster message.from
    updateswarmstatus
    publishswarmstatus
  end

  #updates the internal swarm status
  def updateswarmstatus
    @log.debug "#{@username} - Now updating swarm status."
    @swarm.each do |jid,attributes|
      unless @roster.find(jid).nil?
        @roster.find(jid).each do |roster_jid,node|
          #TODO: I don't know why, but i can't see whether the other nodes are online or not, this SUCKS!
          #@log.debug "#{@username} - Checking if #{roster_jid} is online: #{node.online?}"
          updateswarmerattribute roster_jid,"status","connected" #if node.online?
        end
      end
      if @roster.find(jid).nil?
        updateswarmerattribute jid,"status","unknown"
      end
    end
  end

  #updates a single swarmer attribute
  def updateswarmerattribute jid,attribute,value
    #we need to be a little carefull here
    begin
      @log.debug "#{@username} - Updating #{jid} attribute: #{attribute} with value: #{value}"
      @swarm[jid] = Hash.new("empty swarmer entry") unless @swarm.has_key? jid
      @swarm[jid][attribute] = value
    rescue
      @log.warn "#{@username} - Swarmer attribute #{attribute} couldn't be updated for #{jid}."
    end
  end

  #adds a swarmer to the roster
  def addtoroster jid
    #need to add it, unless it's already in the roster
    if @roster.find(jid).nil?
      @log.debug "#{@username} - JID: #{jid} not in roster, adding."
      requestauth jid
      @roster.add(Jabber::Roster::RosterItem.new(jid,jid,:both,:subscribe))
    end
    unless @roster.find(jid).nil?
      @log.debug "#{@username} - JID: #{jid} in roster."
      @roster.find(jid).each do |jid,rosteritem|
        @log.debug "#{@username} - JID: #{jid} rosteritem #{rosteritem.inspect}"
        rosteritem.each_presence { |presence|  @log.debug "#{@username} - #{jid} presence: #{presence.inspect}"}
      end
      updateswarmerattribute jid,"status","connected"
    end
  end

  #publishes the current swarm status
  def publishswarmstatus
    @log.debug "#{@username} - Now publishing swarm status."
    swarmers = ""
    @swarm.each do |jid,attributes|
      if attributes["status"].eql? "connected"
        swarmers += " " unless swarmers.eql? ""
        swarmers += jid
      end
    end
    publish2node @swarmnode,swarmers
  end

  #subscribe to another jid and his nodes
  def subscribe2jid jid
    begin
      requestauth jid
      #will get us the username from the jid
      username = jid.split("@")[0]
      #@roster.add(Jabber::Roster::RosterItem.new(jid,jid,:both,:subscribe))
      subscribe2node "home/#{@host}/#{username}/updates"
      subscribe2node "home/#{@host}/#{username}/config"
      subscribe2node "home/#{@host}/#{username}/swarm"
      sendconnectmessage jid
      @log.debug "#{@username} - Subscribed to: #{jid}"
    rescue
      @log.error "#{@username} - #{jid} couldn't be added."
    end
  end

  #subscribe to a pubsub node
  def subscribe2node node
    begin
      # subscribe to the node
      @pubsub.subscribe_to(node)
      # set callback for new events
      @pubsub.add_event_callback {|event| handlenodeevent node,event}
      @log.debug "#{@username} - Subscribed to node: #{node}"
    rescue
      @log.error "#{@username} - Couldn't subscribe to : #{node}"
    end
  end

  #gets called when node received an event
  def handlenodeevent node,event
    begin
      event.payload.each do |items|
        items.each do |item|
          #TODO: @log.debug "#{@username} - Node event item: #{item}"
        end
      end
    rescue
      @log.error "#{@username} - Node event error: #{event}"
    end
  end

  #send a status message
  def sendconnectmessage jid
    begin
      @log.debug "#{@username} - Sending connect message."
      to = jid
      #we'll use the SWARM CONNECT later on for parsing, can't think of something simpler now, sorry
      subject = "SWARM CONNECT from #{@username}"
      body = "Added you (#{jid}) to my roster and subscribed to your nodes."
      m = Message.new(to, body).set_type(:normal).set_id('1').set_subject(subject)
      @client.send m
      @log.info "#{@username} - Connect message sent to #{jid}."
    rescue
      @log.error "#{@username} - Connect message couldn't be send to #{jid}."
    end
  end

  #request authorization
  def requestauth jid
    request = Jabber::Presence.new
    request.to = jid
    request.type = :subscribe  # This one!
    @client.send(request)
  end

  #register client at server
  def register
    @client.connect(@host, @port)
    @client.register(@password, {'nick'=>@username,'name'=>@username})
  end

  #create our basic nodes
  def createbasicnodes
    createnode @updatesnode
    createnode @confignode
    createnode @swarmnode
  end

  #create our pubsub node
  def createnode node
    begin
      @pubsub.create_node("home/#{@host}/#{@username}/")
    rescue
      #@log.debug "#{@username} - node #{node} already created, skipping."
    end #if this fails, that normaly means the node has already been created
    begin
      @pubsub.create_node(node)
    rescue #if this fails, that normaly means the node has already been created
      #@log.debug "#{@username} - node #{node} already created, skipping."
    end
  end

  #publish something to one of our nodes
  def publish2node node,text
    # create item
    item = Jabber::PubSub::Item.new
    xml = REXML::Element.new("Update")
    xml.text = text
    item.add(xml)
    #publish item
    #@log.debug "Trying to publish this to node: #{text}"
    @pubsub.publish_item_to(node, item)
    @log.debug "#{@username} - Published this to node: #{text}"
  end

  #show our current roster
  def showroster
    @log.debug "#{@username} - Roster Info:"
    @roster.items.each do |jid,node|
      @log.info "#{@username} - Roster member: #{jid} status: #{node.inspect}"
    end
  end

  #output subscriptions
  def getsubscriptions
    subscriptions = @pubsub.get_subscriptions_from_all_nodes()
    @log.debug "#{@username} - subscriptions: #{subscriptions}"
  end
end
