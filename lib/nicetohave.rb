require 'rubygems'
require 'gst'
require 'gnome2'


#Simple mp3 player implementation
class Player

  attr_accessor :pipeline
  attr_accessor :thread

  def initialize filename
    puts "opening file: #{filename}"
    unless File.exist? filename
      puts "File #{filename} not found, sorry."
    end
    Gst.init
    # create a new pipeline to hold the elements
    @pipeline = Gst::Pipeline.new
    # create a disk reader
    @audiosrc = Gst::ElementFactory.make("filesrc")
    @audiosrc.location = filename
    # now it's time to get the decoder (decodebin is auto-magical)
    @decoder = Gst::ElementFactory.make("decodebin")
    #and an audio convertor
    @audioconverter = Gst::ElementFactory.make("audioconvert")
    # and an audio sink
    @audiosink = Gst::ElementFactory.make("pulsesink")
    # add objects to the main pipeline
    @pipeline.add(@audiosrc, @decoder, @audioconverter, @audiosink)
    # link elements
    @audiosrc >> @decoder
    @audioconverter >> @audiosink
    #now we need to connect the decoder and converter
    @decoder.signal_connect("new-decoded-pad") do |element, pad|
      sink_pad = @audioconverter.get_pad("sink")
      pad.link(sink_pad)
    end
    @pipeline.pause
  end
  #basic player methods
  def play
    @thread = Thread.new do
      @pipeline.play
    end
    @thread.run
    puts "Now playing: #{@audiosrc.location}"
  end
  def pause
    @pipeline.pause
  end
  def stop
    @pipeline.stop
  end
end

#display gstreamer plugins
def display_plugins
  Gst::Registry.default.each_plugin do |plugin|
    puts "plugin: " + plugin.name
  end
end

#A simple Ice client, it's auto-magical
class Iceclient
  attr_accessor :pipeline
  attr_accessor :thread

  def initialize streamurl
    puts "opening url: #{streamurl}"
    Gst.init
    # create a new pipeline to hold the elements
    @pipeline = Gst::Pipeline.new
    # create a disk reader
    @audiosrc = Gst::ElementFactory.make("neonhttpsrc")
    @audiosrc.location = streamurl
    # now it's time to get the decoder (decodebin is auto-magical)
    @decoder = Gst::ElementFactory.make("decodebin")
    #and an audio convertor
    @audioconverter = Gst::ElementFactory.make("audioconvert")
    # and an audio sink
    @audiosink = Gst::ElementFactory.make("pulsesink")
    # add objects to the main pipeline
    @pipeline.add(@audiosrc, @decoder, @audioconverter, @audiosink)
    # link elements
    @audiosrc >> @decoder
    @audioconverter >> @audiosink
    #now we need to connect the decoder and converter
    @decoder.signal_connect("new-decoded-pad") do |element, pad|
      sink_pad = @audioconverter.get_pad("sink")
      pad.link(sink_pad)
    end
    @pipeline.pause
  end
  #basic player methods
  def play
    @thread = Thread.new do
      @pipeline.play
    end
    @thread.run
    puts "Now playing stream: #{@audiosrc.location}"
  end
  def pause
    @pipeline.pause
  end
  def stop
    @pipeline.stop
  end
end
